const express = require("express");
const mysql = require('mysql2/promise');
const config = require("../config");
const router = express.Router();

const connection = async (sql) => {
    const sqlconnection = await mysql.createConnection({host:config.host, user: config.username, password: config.password ,database: config.db});
    const [rows, fields] = await sqlconnection.execute(sql);
    return rows;
} 

router.get("/", async(req, res) => {
    let rows = await connection('SELECT * FROM contacts')
    res.json(rows);
});

router.get("/:id(\\d+)/", async(req, res) => {
    let rows = await connection('SELECT * FROM contacts WHERE owner = ' + req.params.id)
    res.json(rows);
});

module.exports = router;