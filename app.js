const express = require('express');
const helmet = require("helmet");
const cors = require("cors");

let app = express();
let bodyParser = require('body-parser');
let contactsController = require("./controllers/contacts");
let messagesController = require("./controllers/messages");
let userController = require("./controllers/user");
let vehicleController = require("./controllers/vehicle");
let phoneController = require("./controllers/phone");

app.use(helmet());
app.use(cors());
app.options('*', cors());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.json({ limit: "50mb", type: "application/json" }));

app.use("/api/contacts", contactsController);
app.use("/api/messages", messagesController);
app.use("/api/user", userController);
app.use("/api/vehicle", vehicleController);
app.use("/api/phone", phoneController);

app.listen(8080);