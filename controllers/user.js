const express = require("express");
const mysql = require('mysql2/promise');
const config = require("../config");
const router = express.Router();

const connection = async (sql) => {
    const sqlconnection = await mysql.createConnection({host:config.host, user: config.username, password: config.password ,database: config.db});
    const [rows, fields] = await sqlconnection.execute(sql);
    return rows;
} 

router.get("/", async(req, res) => {
    let result = await connection('SELECT * FROM users')
    res.json(result);
});

router.get("/:name", async(req, res) => {
    let result = await connection(`SELECT * FROM users WHERE name = '${req.params.name}'`);
    res.json(result);
});

module.exports = router;