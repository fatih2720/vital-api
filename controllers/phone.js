const express = require("express");
const mysql = require('mysql2/promise');
const config = require("../config");
const router = express.Router();

const connection = async (sql) => {
    const sqlconnection = await mysql.createConnection({host:config.host, user: config.username, password: config.password ,database: config.db});
    const [rows, fields] = await sqlconnection.execute(sql);
    return rows;
} 

router.get("/", async(req, res) => {
    let result = await connection('SELECT * FROM phonesettings')
    res.json(result);
});

router.get("/:name", async(req, res) => {
    let result = await connection(`SELECT * FROM phonesettings WHERE owner = '${req.params.name}'`);
    res.json(result);
});

router.put("/:name", async(req, res) => {
    let userCheck = await connection(`select * from phonesettings WHERE owner = '${req.params.name}'`);
    if (userCheck.length == 0) {
        await connection(`INSERT INTO phonesettings (owner, backgroundurl, mute, phonesize) values ('${req.body.owner}', '${req.body.backgroundurl}', '${req.body.mute}', '${req.body.phonesize}')`);
    }
    else{
        await connection(`UPDATE phonesettings SET backgroundurl = '${req.body.backgroundurl}', mute = '${req.body.mute}', phonesize = '${req.body.phonesize}' WHERE owner = '${req.params.name}'`)
    }
});

module.exports = router;